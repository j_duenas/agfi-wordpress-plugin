<?php

/*
Plugin Name: Auto Generate Featured Images
Plugin URI: http://icongraphics.com
Description: Automatically adds a featured image to new posts based on list of options
Version: 1.0
Author: Icon Graphics
Author URI: http://icongraphics.com
*/

/**
 * Automatically adds a random featured image to new posts.
 * @param [type] $post_id [description]
 */
function add_rand_featured_image( $post_id ){

    $post_thumbnail = get_post_meta( $post_id, $key = '_thumbnail_id', $single = true );
    if( empty( $post_thumbnail ) ) {
        $attachment_id = get_random_featured_image();
        add_post_meta( $post_id, $meta_key = '_thumbnail_id', $meta_value = $attachment_id );
    }
}
add_action( 'save_post', 'add_rand_featured_image');

/**
 * Add "Flag as Featurable" to jpg, png, and gif attachments in media library
 * @param array $form_fields
 * @param object $post
 */
function add_featured_toggle_to_images( $form_fields, $post ) {
    $file_type = get_post_mime_type( $post->ID );

    //If attachment is an image, add "Featured Image" toggle
    if(  $file_type === 'image/jpeg' || $file_type === 'image/png' || $file_type === 'image/gif' ) {
        $checked = (bool) get_post_meta( $post->ID, 'agfi_flag_as_featured', true );
        $checked = ($checked) ? 'checked' : '';
        $form_fields['agfi-flag-as-featured'] = array(
            'label' => 'Flag as Featurable',
            'helps' => 'Include this image in list of randomly selected Featured Images?',
            'value' => $checked,
            'input' => 'html',
        );

        $checkbox_name = "attachments[{$post->ID}][agfi-flag-as-featured]";
        $checkbox_id = "attachments[{$post->ID}][agfi-flag-as-featured]";

        $form_fields['agfi-flag-as-featured']['html'] = "<input type='checkbox' {$checked} name='{$checkbox_name}' id='{$checkbox_id}'>";
    }
    

    return $form_fields;
}

add_filter( 'attachment_fields_to_edit', 'add_featured_toggle_to_images', null, 2 );

/**
 * Saves "flag as featured" meta data
 * @param array $post      
 * @param array $attachment
 */
function add_featured_toggle_to_images_save( $post, $attachment ) {
    
    if( isset( $attachment['agfi-flag-as-featured'] ) ) {
        $checked = ( $attachment['agfi-flag-as-featured'] == 'on' ) ? '1' : '0';
        update_post_meta( $post['ID'], 'agfi_flag_as_featured', $checked );
    }

    return $post;
}

add_filter( 'attachment_fields_to_save', 'add_featured_toggle_to_images_save', null, 2 );

/**
 * Get a random "featurable" image
 * @return int       ID of randomly selected "featurable" attachment
 */
function get_random_featured_image() {
    //Query last 4 posts and add their attachment's IDs to exclusion array
    $previous_posts = get_posts( array(
            'posts_per_page' => get_option( 'posts_per_page' ),
        )
    );

    $previous_post_attachments = array();
    foreach( $previous_posts as $previous_post ){
        $previous_post_attachments[] = get_post_thumbnail_id( $previous_post->ID );
    }

    //Query for a random "featurable" image
    $attachments = get_posts( array(
            'posts_per_page' => 1,
            'meta_key' => 'agfi_flag_as_featured',
            'exclude' => $previous_post_attachments,
            'meta_value' => 1,
            'post_type' => 'attachment',
            'orderby' => 'rand'
        )
    );

    return $attachments[0]->ID;
}

?>